## Star Wars - Survivability Calculator Angular Client

This is a Swagger-generated Angular client for use with https://bitbucket.org/mcsilvio/starwars_api. There is not much interesting to see here.  

The Swagger spec is located in the API project.
